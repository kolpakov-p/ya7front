export default {
  computed: {
    getCalculations() {
      return this.$store.getters['jv/get']({ _jv: { type: 'calculations' } })
    },
    getUser() {
      const users = this.$store.getters['jv/get']({ _jv: { type: 'users' } })
      return users[Object.keys(users)[0]]
    },
    getCurrentEntities() {
      var entities = {}

      if (this.getCurrentCalculationID) {
        const currentCalculation = this.$store.getters['jv/get'](
          'calculations/' + this.getCurrentCalculationID
        )

        if (currentCalculation) {
          const entitiesDirty = currentCalculation.calculationEntities

          for (const key in entitiesDirty) {
            // обрабатываем записи по одной
            if (entitiesDirty[key].serviceId) { // если запись не пуста
              var newEntity = entitiesDirty[key]

              // for (let entityKey of Object.keys(newEntity)) {
              //   if (entityKey === 'data') {
              //     try {
              //       if (typeof newEntity.data === 'string') newEntity.data = JSON.parse(newEntity.data)
              //     } catch (e) {
              //       console.log(e)
              //     }
              //   }
              // }

              entities[entitiesDirty[key].serviceId] = newEntity // добавляем запись в объект, в качестве ключа – ID услуги
            }
          }
        }
      }

      return entities
    },
    getCurrentCalculationID() {
      if (this.getCalculations) {
        for (let index in this.getCalculations) {
          let calculation = this.getCalculations[index]
          if (calculation.isLastUsed === true) return index
        }
        return null
      } else {
        return null
      }
    }
  }
}