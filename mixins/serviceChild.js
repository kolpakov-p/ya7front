import { mapState } from 'vuex'
import Calculator from '@ya7auto/services-calculator'
import LegacyVersionNotification from '~/components/LegacyVersionNotification'
import EntityReset from '~/components/entityReset'

export default {
  components: {
    LegacyVersionNotification,
    EntityReset
  },
    computed: {
      ...mapState({
        bootCompleted: (state) => state.boot.complete
      }),
      getCurrentService() {
        return this.$store.getters['jv/get']('services/'+this.SERVICE_ID)
      },
      getCurrentServiceEntity() {
        return this.getCurrentEntities[this.SERVICE_ID]
      },
      isLegacy() {
        // возвращает true, если текущая версия страницы не является текущей
        if (this.getCurrentServiceEntity !== undefined) {
          const userDataRelevancePoint = new Date(this.getCurrentServiceEntity.updatedAt)
          const pageRelevancePoint = this.RELEVANCE_POINT
          const serviceRelevancePoint = new Date(this.getCurrentService.relevancePoint)

          if (
            pageRelevancePoint >= userDataRelevancePoint
            ||
            serviceRelevancePoint >= userDataRelevancePoint
          ) {
            return true
          }
        }

        return false
      },
      getPrices () {
        if (this.haveNecessaryData) {
          return Calculator(
            this.getCurrentService,
            this.calculatorConditions  
          )
        } else {
          return [null, null]
        }
      },
      getRetailPrice() {
        return this.getPrices[0] // первый элемент массива – розничная цена
      },
      getDiscountedPrice() {
        return this.getPrices[1] // второй элемент массива – цена со скидкой
      }  
    },
    async fetch () {
      // подгружаем информацию о текущей услуге
      await this.$store.dispatch('jv/get', 'services/'+this.SERVICE_ID)
    },
    watch: {
      entityData: {
        deep: true,
        immediate: false,
        handler() {
          if (this.isLegacy) return
          if (this.getRetailPrice !== null) this.saveEntity()
        }
      },
      getRetailPrice (newPrice, oldPrice) {
        if (oldPrice !== null && newPrice === null && this.getCurrentServiceEntity !== undefined) this.deleteEntity()
      },
      getCurrentCalculationID () {
        this.setUserData()
      }
    },
    mounted() {
      if (this.bootCompleted) {
        this.setUserData() // устанавливаем пользовательские данные
      } else {
        this.$watch('bootCompleted', function(complete) {
          if (complete) {
            this.setUserData() // устанавливаем пользовательские данные
          }
        })
      }
    },
    methods: {
      async saveEntity() {
        const serviceID = this.SERVICE_ID

        var entity = {
          _jv: {
            // служебные поля
            type: 'calculation-entities',
          },
          data: JSON.stringify(this.entityData),
          price: this.getDiscountedPrice,
          serviceId: serviceID
        }
  
        if (this.getCurrentServiceEntity === undefined) { // если ранее сохраненной сущности нет
          entity._jv.relationships = { // добавляем отношения
            calculations: {
              data: {
                type: 'calculations',
                id: this.getCurrentCalculationID // получаем ID текущий калькуляции
              }
            }
          }
  
          await this.$store.dispatch('jv/create', entity) // создаем новую

          this.$buefy.toast.open({
            message: 'Услуга добавлена в расчет',
            type: 'is-success'
          })
  
          // обновляем данные о сущностях калькуляции в хранилище
          this.$store.dispatch('jv/get', [
            'calculations',
            { url: '/calculations/'+this.getCurrentCalculationID }
          ])
        } else { // иначе сохраняем имеющуюся
          entity._jv.id = this.getCurrentServiceEntity._jv.id // абсолютный ID сохраняемой сущности (получаем по ID услуги)
  
          this.$store.dispatch('jv/patch', entity) // устанавливаем последний активный расчет
        }
      },
      flushAndDeleteEntity() {
        this.loadEntityDefaultData()
        this.deleteEntity()
        
        this.$buefy.toast.open({
          message: 'Услуга удалена из расчета',
          type: 'is-success'
        })
      },
      deleteEntity() {
        this.$store.dispatch('jv/delete', 'calculation-entities/'+this.getCurrentServiceEntity._jv.id)
      },
      loadEntityDefaultData() {
        this.entityData = this.getDefaultData()
      },
      setUserData() {
        var serviceID = this.SERVICE_ID
        var pageData = this.getDefaultData()
        var pageStructureVersion = this.STRUCTURE_VERSION

        if (this.getCurrentServiceEntity === undefined) { // если расчета для данной услуги еще нет
          this.loadEntityDefaultData() // обнуляем данные // этот пункт также необходим для корректной работы переключений между калькуляциями
          return
        } 

        if (this.isLegacy) return // если у пользователя устаревшие данные - ничего не делаем

        try {
            const userVersion = this.getCurrentServiceEntity.structureVersion // версия структуры в ранее сохраненном расчете
  
            if (userVersion === pageStructureVersion) {
              // если версия данных страницы и данных пользователя совпали (защита от загрузки старых данных на новую страницу)

              // получаем ранее сохраненные данные
              var userData = this.getCurrentServiceEntity.data

              try {
                if (typeof userData === 'string') userData = JSON.parse(userData)
              } catch (e) {
                console.log(e)
              }
               
              const userStructure = Object.keys(userData).sort()
  
              const pageStructure = Object.keys(pageData).sort()

              // сравниваем структуру двух объектов (шаблона и ранее сохраненного)
              if (JSON.stringify(pageStructure) === JSON.stringify(userStructure)) {
                this.entityData = userData // выставляем сохраненные данные, если структуры идентичны
              } else {
                console.log("Saved and current service data structure didn't match")
              }
          }
        } catch (err) {
          console.log(err)
        }
      }
    }
}