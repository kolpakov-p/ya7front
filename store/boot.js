export const state = () => ({
  complete: false,
  failed: false,
  inProgress: false
})

export const mutations = {
  COMPLETE(state) {
    state.complete = true
    state.failed = false
    state.inProgress = false
  },
  FAILED(state) {
    state.failed = true
    state.complete = false
    state.inProgress = false
  },
  IN_PROGRESS(state) {
    state.failed = false
    state.complete = false
    state.inProgress = true
  }
}
