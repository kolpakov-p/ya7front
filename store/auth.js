export const state = () => ({
  loggedIn: false,
  preLogin: false,
  isBusy: false
})

export const getters = {
  LOGGED_IN: (state) => {
    return state.loggedIn
  }
}

export const mutations = {
  TOGGLE_LOGGED_IN(state) {
    state.loggedIn = !state.loggedIn
  },
  TOGGLE_PRELOGIN(state) {
    state.preLogin = !state.preLogin
  }
}

export const actions = {
  TOGGLE_PRELOGIN({ commit }) {
    commit('TOGGLE_PRELOGIN')
  },
  TOGGLE_LOGGED_IN({ commit }) {
    commit('TOGGLE_PRELOGIN')
    commit('TOGGLE_LOGGED_IN')
  }
}
