let env = process.env.NODE_ENV

if (env !== 'staging' && env !== 'development' && env !== 'production')
  env = 'production'

// const base = {
// }

const production = {
  server: {
    host: '0.0.0.0',
    port: 8080
  },
  axios: {
    baseURL: 'https://api.ya7auto.ru'
  }
}

const staging = {
  server: {
    host: '0.0.0.0',
    port: 8080
  },
  axios: {
    baseURL: 'http://api-staging.ya7auto.ru'
  }
}

const development = {
  server: {
    port: 80
  },
  axios: {
    baseURL: 'http://localhost:3000'
  }
}

const config = {
  development: {
    //        ...base,
    ...development
  },
  staging: {
    //        ...base,
    ...staging
  },
  production: {
    //        ...base,
    ...production
  }
}

module.exports = config[env]
