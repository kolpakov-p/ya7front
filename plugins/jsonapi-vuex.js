import { jsonapiModule } from 'jsonapi-vuex'

export default function({ $axios, store }) {
  /* const httpClient = $axios.create({
        baseUrl: 'http://localhost:3001',
        headers: {
            'Accept': 'application/vnd.api+json',
            'Content-Type': 'application/vnd.api+json',
        },
    }) */

  store.registerModule('jv', jsonapiModule($axios))
}
