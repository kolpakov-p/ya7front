class Auth {
  constructor(ctx) {
    this.ctx = ctx
    this.store = this.ctx.store
    this.token = null

    this.fetchToken()

    if (this.token) {
      this.processAuth()
    } else {
      this.requestGuestToken()
    }
  }

  /**
   * Состояния плагина
   * (доступные по this.$auth в приложении)
   */
  // get loggedIn() {
  //   return this.store.state.auth.loggedIn
  // }
  // get preLogin() {
  //   return this.store.state.auth.preLogin
  // }
  // get isBusy() { // признак занятости браузера неким запросом
  //   return this.store.state.auth.isBusy
  // }

  /*
  get isGuest() { // признак того, что пользователь пока не зарегистрирован
    var users = this.store.getters['jv/get']('users') 
    return users[ Object.keys(users)[0] ].isGuest
  }
  */
  get getUser() {
    // данные пользователя
    const users = this.store.getters['jv/get']('users')
    return users[Object.keys(users)[0]] // возвращаем первого
  }

  /**
   * Процесс после аутентификации пользователя
   */

  async processAuth() {
    this.ctx.$axios.setToken(this.token, 'Bearer') // установили токен для axios

    try {
      const user = await this.store.dispatch('jv/get', [
        'users',
        { url: '/users/byToken' }
      ]) // получаем данные пользователя

      if (!user.isGuest) {
        // если зашли не как гость, устанавливаем признак входа
        this.store.commit('auth/TOGGLE_LOGGED_IN')
      }

      const calcs = await this.store.dispatch('jv/get', [
        'calculations',
        { url: '/calculations/?include=calculation-entities' }
      ]) // получаем все расчеты пользователя

      if (Object.keys(calcs).length === 0) {
        // если нет ни одного расчета – создаем новый
        const data = {
          // начальные данные нового расчета
          userId: this.getUser.id,
          isLastUsed: true,
          _jv: {
            type: 'calculations'
          }
        }

        const newCalculation = await this.store.dispatch('jv/create', [
          data,
          { url: '/calculations' }
        ]) // создаем пустой расчет

        this.store.commit('boot/COMPLETE') // установили флаг окончания загрузки данных
      } else {
        // иначе говорим, что все готово
        this.store.commit('boot/COMPLETE') // установили флаг окончания загрузки данных
      }
    } catch (err) {
      console.log(err)
      this.store.commit('boot/FAILED') // устанавливаем флаг ошибки загрузки
      this.resetToken() // сбрасываем токен если не смогли получить пользователя
    }
  }

  /**
   * Заполняем данные пользователя
   */
  // async processUser () {
  //   this.store.dispatch('jv/get', [ 'users', { url: '/users/byToken' }]).then((data) => {
  //     if (!data.isGuest) { // если зашли не как гость, устанавливаем признак входа
  //       this.store.commit('auth/TOGGLE_LOGGED_IN')
  //     }
  //     // TODO: выкидывать исключение если данные не соответствуют ожидаемым (нет необходимых полей)
  //   }).catch((errs) => {
  //     console.log(errs);
  //     this.resetToken(); // сбрасываем токен если не смогли получить пользователя
  //   })
  // }

  /**
   * Заполняем расчеты
   */
  // async processCalculations () {
  //   let calcs = await this.store.dispatch('jv/get', ['calculations', {url: '/calculations/?include=calculation-entities'}]) // получаем все расчеты пользователя

  //   if (Object.keys(calcs).length == 0) { // если нет ни одного расчета – создаем новый
  //     const data = {
  //       userId: this.getUser.id,
  //       _jv: {
  //         type: 'calculations',
  //       }
  //     }

  //     this.store.dispatch('jv/create', [data, { url: '/calculations' }]).then((data) => {
  //       let userData = {
  //         _jv: { type: 'users', id: this.getUser.id },
  //         lastCalculationId: data._jv.id
  //       }

  //       this.store.dispatch('jv/patch', [ userData, {url: '/users/byToken'}]) // устанавливаем последний активный расчет

  //       this.store.commit('auth/BOOT_DATA_LOADED') // установили флаг окончания загрузки данных
  //     }).catch((errs) => {
  //       console.log(errs) // wrap into IF DEV
  //       return false
  //     })
  //   } else { // иначе говорим, что все готово
  //     this.store.commit('auth/BOOT_DATA_LOADED') // установили флаг окончания загрузки данных
  //   }
  // }

  /**
   * Получаем токен из хранилища браузера
   */
  fetchToken() {
    const token = localStorage.getItem('token')

    if (/^([0-9A-Za-z]{64})$/.test(token)) {
      this.token = token
    }
  }

  /**
   * Отправка пароля пользователю
   */
  requestPassword(credential) {
    const data = {
      credential,
      _jv: {
        type: 'otp-request'
      }
    }

    this.store
      .dispatch('jv/create', [data, { url: '/auth/otp/request' }])
      .then((data) => {
        this.store.commit('auth/TOGGLE_PRELOGIN')
        return true
      })
      .catch((errs) => {
        console.log(errs) // wrap into IF DEV
        return false
      })
  }

  /**
   * Проверка пароля из фронта
   */
  verifyPassword(credential, password) {
    const data = {
      password,
      credential,
      _jv: {
        type: 'verify'
      }
    }

    this.store
      .dispatch('jv/create', [data, { url: '/auth/otp/verify' }])
      .then((data) => {
        // TODO: выкидывать исключение если данные не соответствуют ожидаемым (нет необходимых полей)
        // TODO: добавить обработку isAlreadyRegistered, можно отображать на фронте
        // this.resetToken()
        // this.storeToken(data.value)
        this.processAuth()
      })
      .catch((errs) => {
        console.log(errs) // wrap into IF DEV
        return false
      })
  }

  /**
   * Просим гостевой токен
   */
  requestGuestToken() {
    const data = {
      _jv: {
        type: 'tokens'
      }
    }

    this.store
      .dispatch('jv/get', [data, { url: '/auth/guest/token' }])
      .then((data) => {
        this.storeToken(data.value)
        this.processAuth()
      })
      .catch((errs) => {
        console.log(errs) // wrap into IF DEV
        return false // переделать
      })
  }

  /**
   * Сохраняем токен
   */

  storeToken(token) {
    localStorage.setItem('token', token)
    this.token = token
  }

  /**
   * Сбрасываем токен
   */
  resetToken() {
    localStorage.removeItem('token')
    this.ctx.$axios.setToken(false)
  }

  /**
   * Выход из системы
   */
  processLogOut() {
    this.resetToken()
    this.store.commit('auth/TOGGLE_LOGGED_IN')
  }
}

export default (ctx, inject) => {
  inject('auth', new Auth(ctx))
}
