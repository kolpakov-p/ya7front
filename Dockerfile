FROM node:10-alpine

ARG DEPLOY_ENV

ENV APP_ROOT /web
ENV NODE_ENV $DEPLOY_ENV

EXPOSE 8080/tcp

WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

RUN npm ci
RUN npm run build

CMD ["npm", "run", "start"]